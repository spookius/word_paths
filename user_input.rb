class UserInput

	def self.get_file_path(file_path)
		if !File.exist?(file_path)
			p "File path [#{file_path}] does not exist!"
			
		else
			file_path
		end
	end

	def self.validate_words(start_word, end_word)
		if start_word == "" || end_word == ""
			p "Start and End words cannot be blank!"
			
		elsif start_word !~ /^[a-zA-Z]+$/ || end_word !~ /^[a-zA-Z]+$/ 
			p "Please only use alpha characters for the Start and End words"
			
		elsif start_word.length != end_word.length
			p "Please use words with the same number of letters"	
		else
			[start_word, end_word]
		end
	end

end
