# Word Paths

## Using Ruby version 2.2.1p85 (2015-02-26 revision 49769) [x86_64-linux]

Word Path Builder is a program which uses depth first search to find a
word path between two given words, i.e. finding a path from one word to 
another word, changing one letter each step, and each intermediate word 
must be in the a given dictionary file.

The program uses two set recursion levels; first a recursion level of 100
is used to find a path between the two words. If a path is not found, the
recursion level is increased to 1000 and the search begins again.
These sensible recursion limitations are in place as to not trigger 
a SystemStackError.

To run the program, use the following syntax:

**ruby word_path_builder.rb /path/to/dict/file start_word end_word**

i.e. **ruby word_path_builder.rb /usr/share/dict/words cat dog**
