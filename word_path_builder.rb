# Word Paths
# Tom O'Brien
# Septemeber 23, 2015
# Using Ruby version 2.2.1p85 (2015-02-26 revision 49769) [x86_64-linux]

# Word Path Builder is a program which uses depth first search to find a
# word path between two given words, i.e. finding a path from one word to 
# another word, changing one letter each step, and each intermediate word 
# must be in the a given dictionary file.

# The program uses two set recursion levels; first a recursion level of 100
# is used to find a path between the two words. If a path is not found, the
# recursion level is increased to 1000 and the search begins again.
# These sensible recursion limitations are in place as to not trigger 
# a SystemStackError.

require_relative 'user_input'
require_relative 'word'
require 'set'

# A hash to store words and their predecessors
$key_store = {}

$file_path = UserInput.get_file_path(ARGV[0])
get_words = UserInput.validate_words(ARGV[1], ARGV[2])
$start_word = get_words[0]
$end_word = get_words[1]
# Set recursion level relative to the start_word size
$recursion_level = ($start_word.size * 4) + 2

# Store a word's predecessors
$predecessors = {}

# Generate Word list from given dictionary
def generate_word_list(word_length, words_file)
	p "Generating Word List..."
	matched_words = Set.new
	lines = File.new(words_file).readlines
	lines.each do |line|
		line.chomp!
		# Only append a line if the length of the line and the user inputted words match
		if line.length == word_length 
			matched_words << line.downcase # Eliminate duplicate capitalised words
		end
	end
	matched_words
end

# Find successors (words with a one-letter difference) to a given word
def word_successors(word, predecessor, predecessors)
	# if the word path is found
	if word[0] == $end_word
		word_path_found(predecessors, predecessor)
	end
	if word.is_a? String
		p "Generating Word successors for '#{word}'"
	else
		p "Generating Word successors for '#{word[0]}'"
	end

	# Create Word Object for a given word
	unless $key_store.has_key?(word)
		if word.is_a? Array
			word = word[0]
			word = Word.new(word, predecessor)
			# word = Word.new(word[0], predecessor)
		else
			word = Word.new(word, predecessor)
		end
		# Set predecessors for a Word (first Word object will have no predecessors, i.e. the start_word)
		word.set_predecessors(predecessors)
		# Set the successors of a word (words with a one-letter difference)
		word.set_successors(word.generate_successors)
		# Create predecessor key
		$predecessors[word.get_word] = predecessor

		update_key_store(word)
	end

end

def update_key_store(word)
	# start_word has no predecessors
	if word.get_predecessor == ""

		# Assign word successors to the respective key
		$key_store[word.get_word] = word.get_successors
		# For each successor, run word_successor to recursively generate successors for each given Word
		$key_store[word.get_word].each do |successor|
			word_successors(successor, word.get_word, "")
		end	

	else

		select_key = []
		check_word = word.get_word
		check_predecessor = "predecessor"
		# Build key structure for assignment of respective word successors. 
		# The loop will iterate through the $predecessors array until it reaches an empty string (i.e. the start_word has no predecessors, hence empty string)
		while check_predecessor != ""
			select_key.unshift($predecessors[check_word])
			check_predecessor = select_key[0]
			check_word = check_predecessor
		end
	
		# Construct array to pass into eval methods
		select_key_string = ""
		select_key.each do |key|
			unless key == ""
				select_key_string << "['#{key}']"
			end
		end
		if caller.length <= $recursion_level
			p "Recursion level: " + caller.length.to_s
			# Select correct key in $key_store and give a value of the word's successors
			eval("$key_store#{select_key_string}['#{word.get_word}'] = word.get_successors")
			
			# For each successor, run word_successor to recursively generate successors for each given Word
			eval("$key_store#{select_key_string}[word.get_word]").each do |successor|
				word_successors(successor, word.get_word, select_key)
			end	
		end
	end
end

def word_path_found(predecessors, predecessor)
	path_found = "Word Path found! "
	unless predecessors.is_a? String
		predecessors.each do |pred|
			unless pred == "" then path_found += "#{pred} -> " end
		end
	end
	path_found += "#{predecessor} -> #{$end_word}" 
	p path_found
	exit
end


$word_list = generate_word_list($start_word.length, $file_path)
# Start generating word successors
word_successors($start_word, "", "")

# increase recursion level to 100 and start again
p "No Word Paths found at this recursion level (#{$recursion_level}). Increasing level to 1000"
$key_store = {}
$predecessors = {}
$recursion_level = 100
word_successors($start_word, "", "")

# If the word_path_found method is never triggered, the program will eventually terminate here
p "No Word Paths found at recursion level #{$recursion_level}. Deeper search may be possible with larger stack size"
exit