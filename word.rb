class Word

	def initialize(word, predecessor)
		@word = word
		@successors = {}
		@predecessor = predecessor
		@predecessors = []
	end

	def get_word
		@word
	end

	def get_predecessor
		@predecessor
	end

	def set_predecessors(p)
		@predecessors = p
	end

	def set_successors(s)
		@successors = s
	end

	def get_successors
		@successors
	end

	def generate_successors

		successors = {}
		# Iterate through each word in the $word_list provided by the generate_word_list method
		$word_list.each do |dict_word|
			# If true, skip this dict_word matching
			unless check_predecessors(dict_word)
				for index in 0..@word.length-1
					match = match_dict_words(dict_word, index)
					if match
						successors[match] = {}
					end
				end
			end
		end
		successors
	end

	private

	def check_predecessors(dict_word)
		# If the dict_word matches any of this Word's predecessors, return true
		duplicate_counter = 0
		if @predecessor == dict_word
			return true
		end
		unless @predecessors.is_a? String
			@predecessors.each do |pred|
				if pred == dict_word
					duplicate_counter += 1
				end
			end
			if duplicate_counter > 0
				return true
			else
				return false
			end
		end
	end

	def match_dict_words(dict_word, index)
		# Split @word into an array
		word_array = @word.split(//)
		letter = word_array[index]
		# Iterate through the alphabet, replacing the letter at 'index'
		('a'..'z').each do |replacement_letter|
			unless letter == replacement_letter 
				word_array[index] = replacement_letter

				if word_array.join == dict_word
					return dict_word
				end
			end
		end
		return false
	end


end