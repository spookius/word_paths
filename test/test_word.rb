require "test/unit"
require_relative '../word'

class TestWord < Test::Unit::TestCase

	def test_initialize
		assert_instance_of(Word, Word.new("word", "wore"))
		assert_raises(ArgumentError) { Word.new }
	end

	def test_set_predecessors
		assert_equal(["tore", "torn"], Word.new("word", "wore").set_predecessors(["tore", "torn"]))
		assert_not_equal(["torn", "tore"], Word.new("word", "wore").set_predecessors(["tore", "torn"]))
	end

	def test_set_successors
		assert_equal({"tore": "torn"}, Word.new("word", "wore").set_successors({"tore": "torn"}))
		assert_not_equal({"torn": "tore"}, Word.new("word", "wore").set_successors({"tore": "torn"}))
	end

end

