require "test/unit"
require_relative '../user_input'

class TestUserInput < Test::Unit::TestCase

	def test_get_file_path
		assert(true, UserInput.get_file_path("/usr/share/dict/words"))
		assert("File path [/file/does/not/exist] does not exist!", UserInput.get_file_path("/file/does/not/exist"))
		assert("File path [] does not exist!", UserInput.get_file_path(""))
	end

	def test_validate_words
		assert_equal(["word", "path"], UserInput.validate_words("word", "path"))
		assert_equal("Please use words with the same number of letters", UserInput.validate_words("same", "length"))
		assert_equal("Please only use alpha characters for the Start and End words", UserInput.validate_words("word!", "path"))
		assert_equal("Start and End words cannot be blank!", UserInput.validate_words("", ""))
	end

end

